﻿namespace CarLibrary;

public class Truck : Car
{
    public int Capacity { get; protected set; }
    
    public Truck(string brand, string color, float engineCapacity, int speed, int capacity) 
        : base(brand, color, engineCapacity, speed)
    {
        Capacity = capacity > 0 ? capacity : 100;
    }
}