﻿namespace CarLibrary;

public class PassengerCar : Car
{
    public int PassengerNumber { get; protected set; }
    
    public PassengerCar(string brand, string color, float engineCapacity, int speed, int passengerNumber)
        : base(brand, color, engineCapacity, speed)
    {
        PassengerNumber = passengerNumber > 0 ? passengerNumber : 2;
    }
}