﻿using System.Drawing;

namespace CarLibrary;

public abstract class Car
{
    private int _currentSpeed;

    public string Brand { get; protected set; }
    public string Color { get; protected set; }
    public float EngineCapacity { get; protected set; }

    public int CurrentSpeed
    {
        get => _currentSpeed;
        set => _currentSpeed = value >= 0 ? value : 0;
    }

    public Car(string brand, string color, float engineCapacity, int speed)
    {
        (Brand, Color) = (brand, color);
        EngineCapacity = engineCapacity > 0 ? engineCapacity : 0.1f;
        CurrentSpeed = speed;
    }
}