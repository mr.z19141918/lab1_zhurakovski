﻿using CarLibrary;

var truck = new Truck("brand", "green", 2.7f, 0, 1000);

Console.WriteLine($"Brand: {truck.Brand}\nColor: {truck.Color}\nEngineCapacity: {truck.EngineCapacity}\n" +
                  $"Speed: {truck.CurrentSpeed}\nCapacity: {truck.Capacity}");

Console.ReadKey();